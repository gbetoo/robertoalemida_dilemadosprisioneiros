/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho4;

import java.util.BitSet;

/**
 *
 * @author Roberto Almeida
 */
public class Cromossomo {

    private int posReal = 0;// são os contadorres
    private int posInt = 0;
    private int posBol = 0;
    private int tamanho;
    private int tipo; //0 é binário, 1 é inteiro e 2 é real.

    private BitSet genesBin;
    private Double[] genesReal;
    private Integer[] genesInt;

    // Por padrão são 44 pares
    public Cromossomo() {
        this.genesReal = new Double[44];
        this.tipo = 1;
        this.tamanho = 44;
    }

    public Cromossomo(Integer[] genes) {
        this.genesInt = genes;
    }

    public Cromossomo(Double[] genes) {
        this.genesReal = genes;
    }

    public Cromossomo(BitSet genes) {
        this.genesBin = genes;
    }

    public Cromossomo(int tam, int tipoVar) {//tamanho do cromosso (tam array)
        switch (tipoVar) {
            case 0:
                this.genesBin = new BitSet();
                this.tipo = 0;
                this.tamanho = tam;
            case 1:
                this.genesInt = new Integer[tam];
                this.tipo = 1;
                this.tamanho = tam;
            case 2:
                this.genesReal = new Double[tam];
                this.tipo = 2;
                this.tamanho = tam;
        }
    }

    public Object getGene(int posicao) {
        if (tipo == 0) {
            return getGeneBin(posicao);
        } else if (tipo == 1) {
            return getGeneInt(posicao);
        } else {
            return getGeneReal(posicao);
        }
    }

    private Boolean getGeneBin(int posicao) {
        return (Boolean) (this.getGenesBin().get(posicao));
    }

    private Integer getGeneInt(int posicao) {
        return this.getGenesInt()[posicao];
    }

    private Double getGeneReal(int posicao) {
        return this.getGenesReal()[posicao];
    }

    public void addGene(Object g) {
        if (this.tipo == 0) { //boolean

        } else if (this.tipo == 1) {
            addGeneInteger(g);
        } else {
            addGeneReal(g);
        }
    }

    private void addGeneReal(Object g) {
        this.genesReal[posReal] = (Double) g;
        this.posReal++;
    }

    private void addGeneInteger(Object g) {
        this.genesInt[posInt] = (Integer) g;
        this.posInt++;
    }

    /**
     * @return the genesBin
     */
    public BitSet getGenesBin() {
        return genesBin;
    }

    /**
     * @param genesBin the genesBin to set
     */
    public void setGenesBin(BitSet genesBin) {
        this.genesBin = genesBin;
    }

    /**
     * @return the genesReal
     */
    public Double[] getGenesReal() {
        return genesReal;
    }

    /**
     * @param genesReal the genesReal to set
     */
    public void setGenesReal(Double[] genesReal) {
        this.genesReal = genesReal;
    }

    /**
     * @return the genesInt
     */
    public Integer[] getGenesInt() {
        return genesInt;
    }

    /**
     * @param genesInt the genesInt to set
     */
    public void setGenesInt(Integer[] genesInt) {
        this.genesInt = genesInt;
    }
}
