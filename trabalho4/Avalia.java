/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho4;

/**
 *
 * @author Roberto Almeida
 */

/* aguardando especificações
   ela deve receber o Calculo do fitness - private static void avalia(Populacao 
   populacao)que por hora está na classe Main, defenciador de entrada de valor - 
   que deveria estar na pripria classe indivíduo - e o decodificador de indivíduo
   que recebe sua caracteristicas e retornavalores numéricos.
 */
public class Avalia {
    
    public static Populacao Fitness(Populacao pop, int modoAvaliador, int limCadBonus, double bonus) {
        Individuo ind, avaliador;
        int num, cont;
        if (modoAvaliador == 1){//dara o numero de vezes quesera usado o mesmo avaliador
            num = 1;
            cont = num;
        }else{
            num = pop.getTamanho()*(modoAvaliador/100);
            cont = num;
        }
        
        avaliador = pop.getIndividuo(AG.mt.nextInt(pop.getTamanho()-1)+1);
        
        for (int i = 0; i < pop.getTamanho() ; i++) {
            ind = pop.getIndividuo(i);
            //System.out.println("---Antes do fitness [" + (i + 1) + "] fitInd: " 
            //            + ind.getFitInd() + " | FitGroup: " + ind.getFitGroup());
            if (cont == 0){// aqui trocado o avaliador
                avaliador = pop.getIndividuo(AG.mt.nextInt(pop.getTamanho()-1)+1);
                cont = num;
            }
            if (ind.getFitInd() == 0.0){//pra nao reavaliar qualquerindividuo
            ind = fitInd(ind, avaliador);
            ind = fitGroup(ind, avaliador);
            ind = calcBonus(ind, limCadBonus, bonus);
            pop.setIndividuo(i, ind);
            cont--;
            }
            //System.out.println("+++Depois do fitness [" + (i + 1) + "] fitInd: " 
            //            + ind.getFitInd() + " | FitGroup: " + ind.getFitGroup());
        }
            return pop;
        }
        
    private static Individuo fitInd(Individuo ind, Individuo avaliador){
        double fitind = 0;
        String par;
        for (int i = 0; i < ind.getTam(); i++){
            par = convGene(ind.getGene(i)) + convGene(avaliador.getGene(i));
            if (par.equals("DC")){ // 0 anos
                fitind += 1;
            }else if (par.equals("CC")){//0,5 anos
                fitind += 0.33333;
            }else if (par.equals("DD")){//10 anos
                fitind += 0.01667;
            }else if (par.equals("CD")){//30anos
                fitind += 0;
            }
            
        }
        ind.setFitInd(fitind/ind.getTam());// foi adicionado +1 para normalizar a saida
        return ind;
    }
    
    private static Individuo fitGroup(Individuo ind, Individuo avaliador){
        double fitGr = 0;
        String par;
        for (int i = 0; i < ind.getTam(); i++){
            par = convGene(ind.getGene(i)) + convGene(avaliador.getGene(i));
            if (par.equals("CC")){
                fitGr += 1;
            }else if (par.equals("CD")){
                fitGr += 0.5;
            }else if (par.equals("DC")){
                fitGr += 0.5;
            }else if (par.equals("DD")){
                fitGr += 0;
            }
        }
        ind.setFitGru(fitGr/ind.getTam());// foi adicionado +1 para normalizar a saida
        return ind;
    }
    
    private static Individuo calcBonus(Individuo ind,int LimCadeia, double bonus){
        int cont_b = 0;
        double fitind = ind.getFitInd();
        int ContDC = 0;//contador de Delata ou Coopera 
        for (int i = 0; i < ind.getTam(); i++){
            if (ind.getGene(i)< 0.5){
                ContDC++;
                cont_b++;
                if (cont_b >= LimCadeia){
                    fitind += (bonus*fitind);
                    cont_b = 0;
                }
            }
        }
        if (fitind > 1) { fitind = 1;}
        ind.setIndiceDC(ContDC);
        ind.setFitInd(fitind);
        return ind;
    }
    
    private static String convGene(double gene) {
        String p;
        if (gene > 0.5) {
            p = "D";
        } else {
            p = "C";
        }
        return p;
    }
}
 
