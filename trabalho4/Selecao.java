/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho4;


/**
 *
 * @author Roberto Almeida
 */
public class Selecao {
    
    private Populacao pais;
    private Populacao atual;
    
    /**
     *
     * @param p população referente à geração atual.
     * @param modo 0 se é para individuo ou 1 para grupo
     * @param tamRingue tamanho do ringue
     *  
     */
    public Selecao(Populacao p, int modo, int tamRingue) {
        this.pais = new Populacao(p.getTamanho());
        this.atual = p;
        this.selecionaPais(modo, tamRingue);
    }

    private void selecionaPais(int modo, int tamRingue) {
        if (modo == 0){
            for (int posicao = 0; posicao < atual.getTamanho(); posicao++) {
                pais.setIndividuo(posicao, torneio4ind(atual, tamRingue));
            }
        }else{
            for (int posicao = 0; posicao < atual.getTamanho(); posicao++) {
                pais.setIndividuo(posicao, torneio4gru(atual, tamRingue));
            }
        }
    }

    private Individuo torneio4ind(Populacao p, int tamRingue) {;// set param 4 use 
    
        Individuo ind, melhor;
        int num = AG.mt.nextInt(atual.getTamanho()-1)+1;
        melhor = new Individuo(p.getIndividuo(num)); //sorteia o primeiro individuo

        for (int i = 0; i < tamRingue; i++) {// poderia usar colection sort
            num = AG.mt.nextInt(atual.getTamanho()-1)+1;
            ind = new Individuo(p.getIndividuo(num));
            if (ind.getFitInd() > melhor.getFitInd()){
                melhor = ind;
            }
        }
        return melhor;
    }
    
    private Individuo torneio4gru(Populacao p, int tamRingue) {;// set param 4 use 
        
        Individuo ind, melhor;
        int num = AG.mt.nextInt(atual.getTamanho()-1)+1;
        melhor = new Individuo(p.getIndividuo(num)); //sorteia o primeiro individuo

        for (int i = 0; i < tamRingue; i++) {// colection sort
            num = AG.mt.nextInt(atual.getTamanho()-1)+1;
            ind = new Individuo(p.getIndividuo(num));
            if (ind.getFitGru() > melhor.getFitGru()){
                melhor = ind;
            }
        }
        return melhor;
    }

    public Populacao getPais() {
        return this.pais;
    }
}
