/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho4;

import java.io.IOException;
import java.io.OutputStream;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Roberto Almeida
 */
public class Plot {

    private double[][][][] registro;
    private int fim;

    private DefaultCategoryDataset dados1, dados2;
    private JFreeChart grafico1, grafico2;

    public Plot(double[][][][] reg, String titulo, int numGeracoes) {
        this.registro = reg;
        this.fim = numGeracoes;
        this.dados1 = new DefaultCategoryDataset();
        this.dados2 = new DefaultCategoryDataset();
        this.grafico1 = ChartFactory.createLineChart(titulo,
                "Geracoes", "Valores",
                dados1,
                PlotOrientation.VERTICAL,
                true, true, false);
        this.grafico2 = ChartFactory.createBarChart(titulo,
                "geracoes", "individuos",
                dados2,
                PlotOrientation.VERTICAL,
                true, true, true);
    }

    public void plotaG() {
        for (int j = 0; j < AG.execucoes; j++) {
            for (int i = 0; i < fim; i++) {

                double valor = registro[AG.defPlot][j][i][0];//0 é pro melhor
                dados1.addValue(valor, ("Melhor exec " + (j + 1)), ("G[" + (1 + i) + "]"));
                
                valor = registro[AG.defPlot][j][i][1];// 1 é pra media
                dados1.addValue(valor, ("Media exec " + (j + 1)), ("G[" + (1 + i) + "]"));
                
                if ((i == 0) || (i == (fim/2)) || (i == (fim - 1))) {
                    int cont = AG.logContInd[j][i][AG.defPlot];
                    dados2.addValue(cont, ("cooperou exec" + (j + 1)), ("ger["
                            + (1 + i) + "]"));
                    dados2.addValue((AG.tamPopulacao*AG.tamIndividuo) - cont, ("delatou exec"
                            + (j + 1)), ("ger[" + (1 + i) + "]"));
                }
            }
        }

    }

    public void salvar(OutputStream out, int modo) throws IOException {
        if (modo == 0) {
            ChartUtilities.writeChartAsPNG(out, grafico1, 1366, 786);
        } else {
            ChartUtilities.writeChartAsPNG(out, grafico2, 1366, 786);
        }
    }
}
