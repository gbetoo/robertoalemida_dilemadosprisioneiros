/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho4;

/**
 *
 * @author Roberto Almeida
 */
//
public class Individuo {

    private double[] genes;
    private double fit_ind;
    private double fit_gru;
    private int indiceDC; //indice deleta coopera = media dos genes
    private int tamanho;

    public Individuo(Individuo copiado) {
        this.genes = new double[copiado.tamanho];
        for(int i = 0; i < copiado.tamanho; i++) {
            this.genes[i] = copiado.genes[i];
        }
        this.fit_ind = copiado.fit_ind;
        this.fit_gru = copiado.fit_gru;
        this.tamanho = copiado.tamanho;
    }
    
    public Individuo() {
        this.genes = new double[30];
        this.fit_ind = 0;
        this.fit_gru = 0;
        this.tamanho = 30;
    }
    
    public Individuo(int tam) {
        this.genes = new double[tam];
        this.fit_ind = 0;
        this.fit_gru = 0;
        this.tamanho = tam;
    }
    
    public Individuo(double[] genes) {
        
        this.genes = genes;
        this.fit_ind = 0;
        this.fit_gru = 0;
        this.tamanho = genes.length;
    }

    public double[] getGenes() {
        return this.genes;
    }

    public double getGene(int posicao) {
        return this.genes[posicao];
    }

    public double getFitInd() {
        return this.fit_ind;
    }
    
    public double getFitGru() {
        return this.fit_gru;
    }

    public void setGene(int posicao, double val) {
        this.genes[posicao] = val;
    }

    public void setFitInd(double val) {
        this.fit_ind = val;
    }
    
    public void setFitGru(double val) {
        this.fit_gru = val;
    }

    public void setIndividuo(double[] genes) {
        this.genes = genes;
    }
    
    /**
     * @param posicao é a posicao onde se faz a troca do gene
     */
    public void swapGene(int posicao) {
        
        this.genes[posicao] = AG.mt.nextDouble();
    }
    
    public int getTam() {
        return tamanho;
    }

    /**
     * @return the indiceDC
     */
    public int getIndiceDC() {
        return indiceDC;
    }

    /**
     * @param indiceDC the indiceDC to set
     */
    public void setIndiceDC(int indiceDC) {
        this.indiceDC = indiceDC;
    }
}
