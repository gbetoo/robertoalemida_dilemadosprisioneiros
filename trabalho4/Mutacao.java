/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho4;

/**
 *
 * @author Roberto Almeida
 */
public class Mutacao {
    
    public static void fazMutacao(Populacao p, double taxa) {
        
        for (int i = 0; i < p.getTamanho(); i++) {
            Individuo ind = p.getIndividuo(i);
            for (int j = 0; j < p.getIndividuo(0).getTam(); j++) {
                double num = AG.mt.nextDouble();
                if (num < taxa) {
                    ind.swapGene(j);
                }
            }
        }
    }
}
