/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho4;
    
/**
 *
 * @author Roberto Almeida
 */
public class Crossover {  
    
    public static Populacao fazCrossover(Populacao pais, double taxa) {
        Populacao novaGeracao = new Populacao(pais.getTamanho());

        for (int i = 0; i < pais.getTamanho(); i += 2) {
            
            double num = AG.mt.nextDouble();
            double gene;
            Individuo filho1 = new Individuo(pais.getIndividuo(0).getTam());
            Individuo filho2 = new Individuo(pais.getIndividuo(0).getTam());

            if (num < taxa) {
                int posicao = (int) (0 + (AG.mt.nextDouble() * (filho1.getTam() -1 )));
                
                for (int j = 0; j < posicao; j++) {
                    filho1.setGene(j, pais.getIndividuo(i).getGene(j));
                    filho2.setGene(j, pais.getIndividuo(i + 1).getGene(j));
                }

                for (int j = posicao; j < pais.getIndividuo(0).getTam(); j++) {
                    filho1.setGene(j, pais.getIndividuo(i + 1).getGene(j));
                    filho2.setGene(j, pais.getIndividuo(i).getGene(j));
                }
//                for (int j = 0; j < pais.getIndividuo(0).getTam(); j++) {
//                    
//                    gene = pais.getIndividuo(i).getGene(j) + (pais.getIndividuo(i+1).getGene(j) - 
//                            pais.getIndividuo(i).getGene(j)) * taxa;
//                    filho1.setGene(j, gene);
//                    
//                    gene = pais.getIndividuo(i+1).getGene(j) + (pais.getIndividuo(i).getGene(j) - 
//                            pais.getIndividuo(i+1).getGene(j)) * taxa;
//                    filho2.setGene(j, gene);
//                }
                
                novaGeracao.setIndividuo(i, filho1);
                novaGeracao.setIndividuo(i + 1, filho2);

            } else {
                novaGeracao.setIndividuo(i, pais.getIndividuo(i));
                novaGeracao.setIndividuo(i + 1, pais.getIndividuo(i + 1));
            }
        }

        return novaGeracao;
    }

}
