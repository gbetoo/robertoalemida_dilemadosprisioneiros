/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho4;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JFrame;

/**
 * @author Roberto Almeida
 */
public class AG extends JFrame {

    /*  PARAMETROS  */
    public static int execucoes = 5;
    public static int tamIndividuo = 30;
    public static int tamPopulacao = 50;
    private static int numGeracoes = 1000;
    private static int tamRingue = 5;
    private static double taxaCrossover = 0.9;
    private static double taxaMutacao = 0.01;
    private static double bonus = 0.10;
    private static int limCadBonus = 3;
    private static int defAvaliador = 1;
    
    public static MersenneTwister mt = new MersenneTwister();
    
    public static int defPlot = 0; //auxiliar na hora de plotar. se 0 = modo ind, se 1 = modo grupo
    private static double[][][][] logBestPlot = new double[2][execucoes][numGeracoes][2];//1o indice p avalacao por ind ou gru 2 e 4o pro melhor e a media
    public static int[][][] logContInd = new int[execucoes][numGeracoes][2];//2 pois sao um pra ind e um pra group
    

    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        Populacao populacao4ind, populacao4gru, paisInd, paisGru, newGerInd, newGerGru;
        Selecao sInd, sGru;
        Plot gLine, gBarra;
        String cabecalho, ad;
        
        int numCaso = 0;//essa parte somente é para fazer os plots 
//        for (double b=0;b<21;b+=10){
//          bonus = b/100;  
//          for(int lc=3;lc<11;lc++){
//            if((lc==3)||(lc==5)||(lc==10)){
//              limCadBonus = lc;
//              if((b==0)&&((lc==5)||(lc==10))){
//                break;
//              }else{ 
//                for(int av=1;av<51;av++){
//                  if((av==1)||(av==10)||(av==50)){
//                    defAvaliador = av;
//                    numCaso++;// ate aqui
        
        Populacao[][][] registro = new Populacao[2][execucoes][numGeracoes];
        // a variavel registro é só para controle do código. nao neccessaria                  
        
        for (int i = 0; i < execucoes; i++) {
            
            /*sao criados e testado ao mesmo tempo a poulacao para analisar
            individuos e outra para grupo.
            */
            
            //populacao4ind = iniciaPopulacao();
            populacao4gru = iniciaPopulacao();

            //populacao4ind = Avalia.Fitness(populacao4ind, defAvaliador, limCadBonus, bonus);
            populacao4gru = Avalia.Fitness(populacao4gru, defAvaliador, limCadBonus, bonus);
            System.out.println("Execucao " + (i+1));

            // na ordem de acontecimentos...
            for (int j = 0; j < numGeracoes; j++) {
                //System.out.print("G[" + (j + 1) + "] ");

                // seleciona pais
                //sInd = new Selecao(populacao4ind, 0, tamRingue);
                sGru = new Selecao(populacao4gru, 1, tamRingue);
                //paisInd = sInd.getPais();
                paisGru = sGru.getPais();

                // faz cruzamento dos pais e gera uma nova populacao
                //newGerInd = Crossover.fazCrossover(paisInd, taxaCrossover);
                newGerGru = Crossover.fazCrossover(paisGru, taxaCrossover);

                // faz a mutacao nessa nova populacao
                //Mutacao.fazMutacao(newGerInd, taxaMutacao);
                Mutacao.fazMutacao(newGerGru, taxaMutacao);

                // atualiza essa geracao atual
                //populacao4ind = Avalia.Fitness(newGerInd, limCadBonus, defAvaliador);
                populacao4gru = Avalia.Fitness(newGerGru, defAvaliador, limCadBonus, bonus);

                MelhorIndividuo(/*populacao4ind, */populacao4gru, i, j);
                
                
                //registro[0][i][j] = populacao4ind;//regoistro pra ind
                registro[1][i][j] = populacao4gru;//regoistro pra gru
                
            }
        }//fim do AG
        
        // parte resposavel por plotar 
        cabecalho = ("Fator Bonus Caso " + numCaso + ":            "
                + " Bonus: " + bonus + "; Avaliador: 1 para " + 
                defAvaliador + "; Crossover: " + (taxaCrossover*100) + 
                "%; Mutacao: " + (taxaMutacao*100) + "%; " +  " LimCadeiaBonus: "
                + limCadBonus);
        
        System.out.println();
        
//        defPlot = 0;
//        
//        cabecalho = ("FitInd " + cabecalho);
//        
//        g1 = new Plot(logBestPlot, cabecalho, numGeracoes);
//        g1.plotaG();
//        g1.salvar(new FileOutputStream("grafFitInd" + numCaso + "0.png"),1);
//        g1.salvar(new FileOutputStream("grafFitInd" + numCaso + "1.png"),2);
//        System.out.println("Plot do Ind Pronto.");
        defPlot = 1;
        
        
        if (numCaso < 10){ad = "0";}else{ad="";}
        
        gLine = new Plot(logBestPlot, ("FitGru line " + cabecalho), numGeracoes);
        gLine.plotaG();
        gLine.salvar(new FileOutputStream("PlotFitGru" + ad + numCaso + "-Line.png"), 0);
        //gBarra = new Plot(logBestPlot, ("FitGru barra " + cabecalho), numGeracoes);
        gLine.salvar(new FileOutputStream("PlotFitGru" + ad + numCaso + "-Barra.png"), 1);
        System.out.println("Plot do Gru Pronto.");
        
//                  }
//                }
//              }
//            }  
//          }
//        }
        
    }

    private static void MelhorIndividuo(/*Populacao pInd,*/ Populacao pGru, int i, int j) {
        /*dentro dessa funcao sao escolhidor o melhor de cada populacao, salvo a
        media e contados os individuos que cooperam (atraves de suas medias)
        */
        //Individuo ind1 = pInd.getIndividuo(0);
        Individuo ind2 = pGru.getIndividuo(0);
        //double melhor1 = ind1.getFitInd(), media1 = ind1.getFitInd();
        double melhor2 = ind2.getFitInd(), media2 = ind2.getFitInd();
        int posicao1 = 0, posicao2 = 0;
        
        logContInd[i][j][0] = 0; logContInd[i][j][1] = 0; //inicializando os logs contadores dos individuos. se cooperaram
        
        for (int posicao = 1; posicao < tamPopulacao; posicao++) {
            
            //ind1 = pInd.getIndividuo(posicao);
            ind2 = pGru.getIndividuo(posicao);
            //media1 += ind1.getFitInd();
            media2 += ind2.getFitGru();
            
//            if (ind1.getFitInd() > melhor1) {
//                melhor1 = ind1.getFitInd();
//            }
//            if (ind1.getIndiceDC() < 0.5){
//                logContInd[i][j][0]++;
//            }
            if (ind2.getFitGru() > melhor2) {
                melhor2 = ind2.getFitGru();
            }
                logContInd[i][j][1] += ind2.getIndiceDC();       
        }
        
//        logBestPlot[0][i][j][0] = melhor1;
//        logBestPlot[0][i][j][1] = media1 / tamPopulacao;
        logBestPlot[1][i][j][0] = melhor2;
        logBestPlot[1][i][j][1] = media2 / tamPopulacao;
    }

    private static Individuo criaIndividuo() {
        Individuo ind = new Individuo(tamIndividuo);
        for (int j = 0; j < tamIndividuo; j++) {
            //cria um valor aleatorio pro gene que sera criado
            ind.setGene(j, mt.nextDouble());
        }
        return ind;
    }

    /* esta funcao inicia uma nova populacao */
    private static Populacao iniciaPopulacao() {
        Populacao pop = new Populacao(tamPopulacao);
        for (int i = 0; i < tamPopulacao; i++) {
            pop.setIndividuo(i, criaIndividuo());
        }
        return pop;
    }
    
    private static void imprimePopulacao(Populacao pop){
        //imprime todos os fitness
        for (int j = 0; j < tamPopulacao; j++) {
            Individuo ind = pop.getIndividuo(j);
            System.out.println("Ind [" + (j + 1) + "]: " + ind.getFitInd());
        }
        System.out.println();
    }
}
